# Binary Classification Example #

Storyline for the practical session in R of the introduction to machine learning course.

The purpose is to provide a guidelines and let anyone experiment at any modelling step.


### Intro ###

We will be using R in this session. Focus is on the ML principles, not programming language.

Main packages are:

- `caret` http://topepo.github.io/caret/index.html
- `data.table` https://cran.r-project.org/web/packages/data.table/vignettes/datatable-intro.html
- `DataExplorer` https://towardsdatascience.com/simple-fast-exploratory-data-analysis-in-r-with-dataexplorer-package-e055348d9619
- `Hmisc` http://biostat.mc.vanderbilt.edu/wiki/Main/Hmisc
- `tidyverse` https://www.tidyverse.org

Script `./init.R` sets the stage.

### Business Understanding ###

**Situation:**
The response rates of a marketing campaign for bank term deposit are decreasing. While the costs of contact increases due to salary growth in the call center.

**Complication:**
Focus on customers with high probability of subscribing a term deposit sounds like a plan. However, marketing department do not know who they are and how they look like.

**Resolution:**
Build a model evaluating the likelihood of accepting the offer to optimise the target group of the campaign. Both questions: (1) *who* should be in the target group and (2) *how big* should the target group be should to be answered.


#### Data ####

**Business problem _always_ defines what data are needed!** Not the other way around.

Our data is related with direct marketing campaigns of a Portuguese banking institution. The marketing campaigns were based on phone calls. Often, more than one contact to the same client was required, in order to access if the product (bank term deposit) would be ('yes') or not ('no') subscribed.

The goal is to predict if the client will subscribe (yes/no) a term deposit (variable y).

Data are available at https://archive.ics.uci.edu/ml/datasets/Bank+Marketing#.


### Data Understanding ###

Also called EDA (exploratory data analysis) or data profiling.

This step is often repetitive because in real life data quality issues are discovered and ideas for new data sources or features come to mind.

**Data quality**

- number of observations
- missing values; but also `'unknown'`, `'NA'`, `'NULL'`, `'<missing>'` etc.
- outliers

**Basic stats**

- mean, var, sd, quantiles
- skewness and kurtosis

**Visual exploration**

- boxplot, histogram, scatter
- per feature; feature / target

**Modelling related**

- outliers
- distribution (normal, other)
- zero variance and near-zero variance tests
- suggestion for box-cox and/or other transformations
- missing values imputation
- correlated features
- lineary dependent features

Since the task is highly repetitive data scientists tend have their own package to do that.

- `DataExplorer` https://towardsdatascience.com/simple-fast-exploratory-data-analysis-in-r-with-dataexplorer-package-e055348d9619
- `Rprofiling` https://github.com/isaac34mi/Rprofiling
- and many more

Script `./data_understanding.R` contains some basic data exploration.

### Data Preparation ###

Data preparation typically contains:

- Creating dummy variables
- Adding intercept
- Removing Zero- and Near Zero-Variance features
- Identifying correlated features
- Dealing with linear combinations
- Centering and scaling
- Normalisation, Box-Cox, ...
- Imputation
- Other feature transformations like e.g. PCA

Please mind that all these steps need to be fitted on the train data only. However, they need to be applied to any data (train, validate, new) before modelling!

Therefore the data splitting needs to happen at this step too.

- Should the data be split randomly? Or based on the target? Or the features?
- How to split time series?
- What should be the split ratio? How big samples are needed for training and for testing?
- How to deal with imbalanced targets?

For that and following part see `./main.R`. Draft of a scoring script is in `./scoring.R`

### Modelling ###

The 'Kaggle part'.

- Further feature engineering and selection
- So many models available (trees, regressions, neural networks, ...)
- Combination of models (boosting, bagging, ...)
- Hyper-parameter tuning

### Evaluation ###

Our task is binary classification so we should analyse

- confusion matrix
- related measures (accuracy, sensitivity, specificity)
- ROC and AUC
- Lift curves

### Deployment ###

Once we have the model we need to make sure the modelling and scoring can run automatically in production. This is where most of data science beginners struggle.

Focus for instance on:

- version control
- unit tests
- automated deploy
- error handling
- documentation
- development outside production

https://blog.alookanalytics.com/2017/03/08/8-simple-ways-how-to-boost-your-coding-skills-not-just-in-r/

### So what? ###

Does that solve the original business problem? How would you define the target group? And how big it should be?

See https://blog.alookanalytics.com/2017/02/21/propensity-modelling-and-how-it-is-relevant-for-modern-marketing/.

### What next? ###

Data science is a craft. So one need to spend time working on a lot of (test) projects. Experiment with tools, packages, algorithms, features, ...
